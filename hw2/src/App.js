import './App.css';
import * as actions from './actions'
import { connect } from 'react-redux'

function App(props) {

  const handleChange = (input) => {
    if (input > 0 && props.cid < props.userlist.length - 1) {
      props.changeUser(props.cid + 1)
    } else if (input < 0 && props.cid > 0) {
      props.changeUser(props.cid - 1)
    }
  }

  return (
    <div className="App">
      {
        props.userlist.length !== 0 ? (
          <>
            <>
              <img src={props.userlist[props.cid].picture} />
            </>
            <>
              <p>email: {props.userlist[props.cid].email}</p>
            </>
            <>
              <p>gender: {props.userlist[props.cid].gender}</p>
            </>
            <>
              <p>{props.userlist[props.cid].fullname}</p>
            </>
          </>
        ) : (false)
      }
      <div>
        <button onClick={() => handleChange(-1)}>&lt;&lt;</button>
        <button onClick={() => FetchUser(props)}>Generate User</button>
        <button onClick={() => handleChange(1)}>&gt;&gt;</button>
      </div>
    </div>
  );
}

const FetchUser = (props) => {

  fetch('https://randomuser.me/api/')
    .then(response => response.json())
    .then(data => props.addUser(data.results[0]));
}

const mapStateToProps = (state) => ({
  userlist: state.userlist,
  cid: state.cid
})

export default connect(mapStateToProps, actions)(App)


