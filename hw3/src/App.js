import './App.css';
import { connect } from 'react-redux'
import { Component } from "react";

class Redux extends Component {

  handleChange = (input) => {
    if (input > 0 && this.props.cid < this.props.userlist.length - 1) {
      this.props.dispatch({
        type: 'SET_CID',
        cid: this.props.cid + 1
      })
    } else if (input < 0 && this.props.cid > 0) {
      this.props.dispatch({
        type: 'SET_CID',
        cid: this.props.cid - 1
      })
    }
  }

  FetchUser = () => {
    fetch('https://randomuser.me/api/')
      .then(response => response.json())
      .then(data => this.props.dispatch({
          type: 'ADD_USER',
          id: this.props.userlist.length,
          email: data.results[0].email,
          gender: data.results[0].gender,
          fullname: `${data.results[0].name.title} ${data.results[0].name.first} ${data.results[0].name.last}`,
          picture: data.results[0].picture.large
        }
      ));
  }

  render() {
    return (
      <div className="App">
        {
          this.props.userlist.length !== 0 ? (
            <>
              <>
                <img src={this.props.userlist[this.props.cid].picture} />
              </>
              <>
                <p>email: {this.props.userlist[this.props.cid].email}</p>
              </>
              <>
                <p>gender: {this.props.userlist[this.props.cid].gender}</p>
              </>
              <>
                <p>{this.props.userlist[this.props.cid].fullname}</p>
              </>
            </>
          ) : (false)
        }
        <div>
          <button onClick={() => this.handleChange(-1)}>&lt;&lt;</button>
          <button onClick={() => this.FetchUser()}>Generate User</button>
          <button onClick={() => this.handleChange(1)}>&gt;&gt;</button>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  userlist: state.userlist,
  cid: state.cid
})

export default connect(mapStateToProps)(Redux);


